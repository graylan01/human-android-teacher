import logging
import os
import tkinter as tk
import torch

from collections import Counter
from tkinter import scrolledtext, messagebox, StringVar, OptionMenu, IntVar, Radiobutton
from nltk.tokenize import word_tokenize
import aiosqlite
import nltk

from TTS.api import TTS
from llama_cpp import Llama

nltk.data.path.append("/root/nltk_data")
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

DB_NAME = "local_responses.db"
DATA_DIR = "/data/"

LANGUAGES = {
    "english": "english.json",
    "spanish": "spanish.json",
    "hindi": "hindi.json",
    "chinese": "chinese.json",
    "arabic": "arabic.json",
    "portuguese": "portuguese.json",
}

class MultilingualApp:
    def __init__(self, user_identifier, llama_model_path):
        self.user_id = user_identifier
        self.loop = asyncio.get_event_loop()
        self.setup_gui()
        self.response_queue = queue.Queue()
        self.interaction_history = []
        self.lesson_plan = ""
        self.language = "english"

        self.check_or_install_nltk_models()  # Check and install NLTK models

        self.llm = Llama(model_path=llama_model_path, n_gpu_layers=-1, n_ctx=3900)

        # Using default TTS model provided by the TTS library
        self.tts = TTS().to(torch.device("cpu"))  # Assuming CPU usage for TTS
        self.tts_enabled = True
        self.voice_type = "male"

        self.loop.create_task(self.retrieve_past_interactions())
        self.loop.run_until_complete(self.connect_to_db())
        self.load_saved_lesson_plan()

    def check_or_install_nltk_models(self):
        try:
            nltk.data.find('tokenizers/punkt')
        except LookupError:
            logger.info("NLTK models not found. Downloading...")
            nltk.download('punkt')

    async def connect_to_db(self):
        self.db_conn = await aiosqlite.connect(DB_NAME)
        await self.db_conn.execute("""
            CREATE TABLE IF NOT EXISTS lesson_plans (
                user_id TEXT PRIMARY KEY,
                lesson_plan TEXT
            )
        """)
        await self.db_conn.commit()

    def setup_gui(self):
        self.root = tk.Tk()
        self.root.title("Multilingual AI Lesson Plan Generator")
        self.root.geometry("800x600")

        self.language_label = tk.Label(self.root, text="Select Language:")
        self.language_label.pack(pady=10)

        self.language_var = StringVar(self.root)
        self.language_var.set("english")

        self.language_menu = OptionMenu(self.root, self.language_var, *LANGUAGES.keys(), command=self.change_language)
        self.language_menu.pack()

        self.start_button = tk.Button(self.root, text="Start AI Lesson Plan", command=self.start_lesson_plan)
        self.start_button.pack(pady=10)

        self.save_button = tk.Button(self.root, text="Save Lesson Plan", command=self.save_lesson_plan)
        self.save_button.pack(pady=10)

        self.lesson_label = tk.Label(self.root, text="Lesson Plan:")
        self.lesson_label.pack(pady=10)

        self.lesson_text = scrolledtext.ScrolledText(self.root, height=20, wrap=tk.WORD)
        self.lesson_text.pack(padx=20, fill=tk.BOTH, expand=True)

        self.tts_frame = tk.Frame(self.root, bd=2, relief=tk.SUNKEN)
        self.tts_frame.pack(pady=10, padx=10, fill=tk.X)

        self.tts_enabled_var = IntVar(self.root, value=1)
        self.tts_enabled_check = tk.Checkbutton(self.tts_frame, text="Enable TTS", variable=self.tts_enabled_var, command=self.toggle_tts)
        self.tts_enabled_check.pack(side=tk.LEFT, padx=10)

        self.voice_label = tk.Label(self.tts_frame, text="Select Voice:")
        self.voice_label.pack(side=tk.LEFT, padx=10)

        self.voice_var = StringVar(self.root)
        self.voice_var.set("male")

        self.male_radio = Radiobutton(self.tts_frame, text="Male", variable=self.voice_var, value="male", command=self.change_voice)
        self.male_radio.pack(side=tk.LEFT)

        self.female_radio = Radiobutton(self.tts_frame, text="Female", variable=self.voice_var, value="female", command=self.change_voice)
        self.female_radio.pack(side=tk.LEFT)

        self.tts_button = tk.Button(self.root, text="Text-to-Speech", command=self.text_to_speech)
        self.tts_button.pack(pady=10)

    def change_language(self, event=None):
        self.language = self.language_var.get()

    def toggle_tts(self):
        self.tts_enabled = bool(self.tts_enabled_var.get())

    def change_voice(self):
        self.voice_type = self.voice_var.get()

    def start_lesson_plan(self):
        self.lesson_plan = ""
        self.generate_lessons()

    def generate_lessons(self):
        self.lesson_text.delete("1.0", tk.END)
        self.loop.create_task(self.generate_lesson_plan_async())

    async def generate_lesson_plan_async(self):
        try:
            lesson_counter = 0
            self.lesson_plan = ""
            max_tokens = 100

            while lesson_counter < 15:
                lesson_counter += 1
                lesson_text = await self.generate_lesson(max_tokens)
                if lesson_text:
                    self.update_output(lesson_text)
                    self.lesson_plan += lesson_text
                    max_tokens = self.adjust_max_tokens(max_tokens, lesson_text)
                else:
                    logger.warning(f"Empty lesson text generated for lesson {lesson_counter}")

            if not self.check_content_uniqueness(self.lesson_plan):
                messagebox.showwarning("Duplicate Content", "Lesson plan contains duplicate content.")

        except Exception as e:
            logger.error(f"Error generating lesson plan: {e}")
            messagebox.showerror("Error", f"Error generating lesson plan: {e}")

    async def generate_lesson(self, max_tokens):
        try:
            lesson_chunks = []
            max_words = 10000
            current_words = 0

            while current_words < max_words:
                prompt, objectives = self.generate_prompt_and_objectives()
                llama_text = await self.llm_generate(prompt, max_tokens)
                if llama_text:
                    if self.is_overlapping_content(lesson_chunks, llama_text):
                        logger.warning("Overlapping content detected. Generating new lesson text.")
                        continue
                    lesson_chunks.append(llama_text)
                    current_words += len(word_tokenize(llama_text))
                else:
                    logger.warning("Generated empty text, retrying.")

            lesson_text = ' '.join(lesson_chunks)
            return lesson_text

        except Exception as e:
            logger.error(f"Error generating lesson: {e}")
            return ""

    async def llm_generate(self, prompt, max_tokens=100):
        try:
            inputs = await asyncio.to_thread(self.llm, prompt, max_tokens=max_tokens)
            if inputs and isinstance(inputs, dict) and 'choices' in inputs:
                return inputs['choices'][0]['text']
            else:
                return ""
        except Exception as e:
            logger.error(f"Error generating text with LLM: {e}")
            return ""

    def adjust_max_tokens(self, current_max_tokens, lesson_text):
        word_count = len(word_tokenize(lesson_text))
        if word_count < 50:
            return min(current_max_tokens + 20, 200)
        elif word_count > 200:
            return max(current_max_tokens - 20, 50)
        else:
            return current_max_tokens

    def is_overlapping_content(self, lesson_chunks, new_text):
        new_text_words = set(word_tokenize(new_text.lower()))
        for chunk in lesson_chunks:
            chunk_words = set(word_tokenize(chunk.lower()))
            overlap = new_text_words & chunk_words
            if len(overlap) / len(new_text_words) > 0.3:
                return True
        return False

    def generate_prompt_and_objectives(self):
        prompts_file = LANGUAGES[self.language]
        prompts = self.load_prompts(prompts_file)
        prompt_data = prompts[len(self.lesson_plan) % len(prompts)]
        prompt = prompt_data["description"]
        objectives = prompt_data["objectives"]
        return prompt, objectives

    def load_prompts(self, prompts_file):
        try:
            with open(prompts_file, 'r', encoding='utf-8') as f:
                prompts_data = json.load(f)
            return prompts_data["prompts"]
        except Exception as e:
            logger.error(f"Error loading prompts from {prompts_file}: {e}")
            messagebox.showerror("Error", f"Error loading prompts from {prompts_file}: {e}")
            return []

    def text_to_speech(self):
        try:
            if self.tts_enabled and self.lesson_plan:
                voice = "male" if self.voice_type == "male" else "female"
                self.tts.tts_to_file(
                    text=self.lesson_plan,
                    speaker=voice,
                    file_path="lesson_plan.wav"
                )
                logger.info("TTS audio generated successfully.")
                messagebox.showinfo("TTS", "Text-to-speech audio generated successfully.")
            else:
                messagebox.showwarning("TTS", "TTS is disabled or lesson plan is empty.")
        except Exception as e:
            logger.error(f"Error generating TTS: {e}")
            messagebox.showerror("TTS Error", f"Error generating TTS: {e}")

    def save_lesson_plan(self):
        self.loop.create_task(self.save_lesson_plan_async())

    async def save_lesson_plan_async(self):
        try:
            if self.lesson_plan:
                await self.db_conn.execute("REPLACE INTO lesson_plans (user_id, lesson_plan) VALUES (?, ?)", (self.user_id, self.lesson_plan))
                await self.db_conn.commit()
                messagebox.showinfo("Save Lesson Plan", "Lesson plan saved successfully.")
            else:
                messagebox.showwarning("Save Lesson Plan", "Lesson plan is empty.")
        except Exception as e:
            logger.error(f"Error saving lesson plan: {e}")
            messagebox.showerror("Error", f"Error saving lesson plan: {e}")

    def load_saved_lesson_plan(self):
        self.loop.create_task(self.load_saved_lesson_plan_async())

    async def load_saved_lesson_plan_async(self):
        try:
            cursor = await self.db_conn.execute("SELECT lesson_plan FROM lesson_plans WHERE user_id = ?", (self.user_id,))
            row = await cursor.fetchone()
            if row and row[0]:
                self.lesson_plan = row[0]
                self.lesson_text.insert(tk.END, self.lesson_plan)
                logger.info("Loaded saved lesson plan successfully.")
            else:
                logger.info("No saved lesson plan found for this user.")
        except Exception as e:
            logger.error(f"Error loading saved lesson plan: {e}")

    async def retrieve_past_interactions(self):
        try:
            cursor = await self.db_conn.execute("SELECT interaction_data FROM user_interactions WHERE user_id = ?", (self.user_id,))
            async for row in cursor:
                self.interaction_history.append(row[0])
        except Exception as e:
            logger.error(f"Error retrieving past interactions: {e}")

    def update_output(self, new_text):
        self.lesson_text.insert(tk.END, new_text + "\n\n")
        self.lesson_text.see(tk.END)

    def check_content_uniqueness(self, text):
        words = word_tokenize(text.lower())
        word_counts = Counter(words)
        duplicate_count = sum(1 for count in word_counts.values() if count > 1)
        return duplicate_count / len(word_counts) < 0.2

    def run(self):
        self.root.mainloop()

def main():
    user_id = "student"
    llama_model_path = "/data/llama-2-7b-chat.ggmlv3.q8_0.bin"
    app = MultilingualApp(user_id, llama_model_path)
    app.run()
    
if __name__ == "__main__":
    main()