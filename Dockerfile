# Use an appropriate Python base image
FROM python:3.11-slim-buster

# Set environment variables
ENV DEBIAN_FRONTEND noninteractive

# Install necessary system packages, including Python development tools, tkinter, and other dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    python3-dev \
    python3-tk \
    libgl1-mesa-glx \
    curl \
 && rm -rf /var/lib/apt/lists/*

# Upgrade pip and install wheel globally
RUN pip install --no-cache-dir --upgrade pip wheel

# Set the working directory inside the container
WORKDIR /app

# Copy requirements.txt to the working directory
COPY requirements.txt .

# Install Python dependencies excluding TTS
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application files to the working directory
COPY . .

# Install NLTK version 3.8.1 (or any desired version)
RUN pip install --no-cache-dir nltk==3.8.1

# Script to download the model file and install TTS if not already present
RUN echo '#!/bin/bash\n\
if [ ! -f /data/llama-2-7b-chat.ggmlv3.q8_0.bin ]; then\n\
  echo "Downloading model file..."\n\
  curl -L -o /data/llama-2-7b-chat.ggmlv3.q8_0.bin https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGML/resolve/main/llama-2-7b-chat.ggmlv3.q8_0.bin --progress-bar\n\
  echo "Verifying model file..."\n\
  echo "3bfdde943555c78294626a6ccd40184162d066d39774bd2c98dae24943d32cc3  /data/llama-2-7b-chat.ggmlv3.q8_0.bin" | sha256sum -c -\n\
else\n\
  echo "Model file already exists, skipping download."\n\
fi\n\
ls -lh /data/llama-2-7b-chat.ggmlv3.q8_0.bin\n\
\n\
# Install TTS if not already installed and cache in /data/tts_cache directory
TTS_CACHE_DIR="/data/tts_cache"\n\
if ! python -c "import tts" &> /dev/null; then\n\
  echo "Installing TTS..."\n\
  pip install --no-cache-dir -t $TTS_CACHE_DIR tts==0.22.0\n\
else\n\
  echo "TTS already installed."\n\
fi\n\
' > /app/setup_and_run.sh

# Make the script executable
RUN chmod +x /app/setup_and_run.sh

# Command to run the GUI application with X11 Forwarding, including model download and TTS setup
CMD ["bash", "-c", "export DISPLAY=:0 && /app/setup_and_run.sh && python /app/main.py"]
