 

# Humoid Teacher

Humoid Teacher is an AI-powered application that generates multilingual lesson plans using natural language processing techniques.

## Features

- Generates lesson plans in multiple languages based on user prompts.
- Utilizes AI model for text generation (LLAMA model).
- Supports text-to-speech functionality for generated lesson plans.

## Requirements

- Docker
- Python 3.11 or higher

## Installation

### Building the Docker Image

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/graylan01/human-android-teacher.git
   cd human-android-teacher
   ```

2. Build the Docker image:
   ```bash
   docker build -t humoid-teacher .
   ```

### Running the Docker Container

To run the Humoid Teacher application with persistence for the SQLite database:

```bash
docker run --rm -it \
    --net=host \
    --env="DISPLAY" \
    --env="PULSE_SERVER=unix:/run/user/1000/pulse/native" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume="/run/user/1000/pulse:/run/user/1000/pulse" \
    --volume="$HOME/humoid_data:/data" \
    --volume="$HOME/humoid_data/nltk_data:/root/nltk_data" \
    --volume="$HOME/humoid_data/weaviate:/root/.cache/weaviate-embedded" \
    --volume="$HOME/humoid_data/tts_cache:/data/tts_cache" \
    --volume="$HOME/humoid_data/tts_models:/data/tts_models" \
    humoid-teacher:latest

```

### Explanation:

- **`-v $HOME/humoid_db:/app/data`**: This option mounts a directory (`$HOME/humoid_db`) on your host machine to `/app/data` inside the container. Replace `$HOME/humoid_db` with the path to a directory on your host where you want to store the SQLite database persistently.

## Usage

1. Once the container is running, select your desired language from the dropdown menu.
2. Click on "Start AI Lesson Plan" to generate a lesson plan.
3. The generated lesson plan will appear in the text area.
4. Optionally, click "Save Lesson Plan" to save the generated lesson plan.
5. Click "Text-to-Speech" to listen to the generated lesson plan.

## License

This project is licensed under the GNU General Public License v2.0 - see the [LICENSE](LICENSE) file for details.

---

### Notes:

- **Persistence**: Ensure that the directory you mount (`$HOME/humoid_db` in this example) exists and that the Docker container has read and write access to it.
- **Environment Variables**: Adjust paths and environment variables (`$HOME`) as per your local setup and requirements.
- **Security**: Consider the implications of mounting directories and handling sensitive data, especially in production environments.

# Docker Install

### Installing Docker

#### English:

1. **Check System Requirements:**
   Ensure your system meets Docker's requirements. Docker runs on Linux, macOS, and Windows.

2. **Download Docker Desktop:**
   - **For Windows and macOS:** Go to [Docker Desktop download page](https://www.docker.com/products/docker-desktop) and download the installer.
   - **For Linux:** Follow the installation instructions specific to your Linux distribution ([Docker Engine](https://docs.docker.com/engine/install/) or [Docker Desktop for Linux](https://www.docker.com/products/docker-desktop)).

3. **Install Docker Desktop:**
   - **Windows:** Double-click the installer and follow the prompts to install Docker Desktop.
   - **macOS:** Double-click the downloaded `.dmg` file, drag Docker to Applications, and follow the installation instructions.
   - **Linux:** Follow the installation instructions provided for your distribution.

4. **Start Docker:**
   - **Windows and macOS:** Docker Desktop should start automatically after installation.
   - **Linux:** Start Docker service using `sudo systemctl start docker` and enable it to start on boot using `sudo systemctl enable docker`.

5. **Verify Installation:**
   Open a terminal or command prompt and run `docker --version` to verify Docker is installed correctly. Also, run `docker run hello-world` to verify Docker can pull and run images.

#### Español:
# Humoid Teacher

Humoid Teacher es una aplicación impulsada por inteligencia artificial que genera planes de lecciones multilingües utilizando técnicas de procesamiento de lenguaje natural.

## Características

- Genera planes de lecciones en varios idiomas basados en indicaciones de usuario.
- Utiliza un modelo de IA para la generación de texto (modelo LLAMA).
- Admite funcionalidad de texto a voz para los planes de lecciones generados.

## Requisitos

- Docker
- Python 3.11 o superior

## Instalación

### Construcción de la imagen de Docker

1. Clona el repositorio:
   ```bash
   git clone https://gitlab.com/graylan01/human-android-teacher.git
   cd human-android-teacher
   ```

2. Construye la imagen de Docker:
   ```bash
   docker build -t humoid-teacher .
   ```

### Ejecución del contenedor de Docker

Para ejecutar la aplicación Humoid Teacher con persistencia para la base de datos SQLite:

```bash
docker run --rm -it \
    --net=host \
    --env="DISPLAY" \
    --env="PULSE_SERVER=unix:/run/user/1000/pulse/native" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume="/run/user/1000/pulse:/run/user/1000/pulse" \
    --volume="$HOME/humoid_data:/data" \
    --volume="$HOME/humoid_data/nltk_data:/root/nltk_data" \
    --volume="$HOME/humoid_data/weaviate:/root/.cache/weaviate-embedded" \
    --volume="$HOME/humoid_data/tts_cache:/data/tts_cache" \
    --volume="$HOME/humoid_data/tts_models:/data/tts_models" \
    humoid-teacher:latest

```

### Explicación:

- **`--volume="$HOME/humoid_data:/data"`**: Esta opción monta el directorio `$HOME/humoid_data` de tu máquina anfitriona dentro del contenedor en `/data`. Asegúrate de reemplazar `$HOME/humoid_data` con la ruta al directorio en tu máquina anfitriona donde deseas almacenar los datos persistentemente.
  
## Uso

1. Una vez que el contenedor esté en ejecución, selecciona tu idioma deseado desde el menú desplegable.
2. Haz clic en "Start AI Lesson Plan" para generar un plan de lección.
3. El plan de lección generado aparecerá en el área de texto.
4. Opcionalmente, haz clic en "Save Lesson Plan" para guardar el plan de lección generado.
5. Haz clic en "Text-to-Speech" para escuchar el plan de lección generado.

## Licencia

Este proyecto está bajo la Licencia Pública General de GNU v2.0 - consulta el archivo [LICENSE](LICENSE) para más detalles.

---

### Notas:

- **Persistencia**: Asegúrate de que el directorio que montas (`$HOME/humoid_data` en este ejemplo) exista y que el contenedor de Docker tenga permisos de lectura y escritura.
- **Variables de entorno**: Ajusta las rutas y las variables de entorno (`$HOME`) según tu configuración local y requerimientos.
- **Seguridad**: Considera las implicaciones de montar directorios y manejar datos sensibles, especialmente en entornos de producción.
1. **Verificar Requisitos del Sistema:**
   Asegúrate de que tu sistema cumpla con los requisitos de Docker. Docker funciona en Linux, macOS y Windows.

2. **Descargar Docker Desktop:**
   - **Para Windows y macOS:** Visita la [página de descarga de Docker Desktop](https://www.docker.com/products/docker-desktop) y descarga el instalador.
   - **Para Linux:** Sigue las instrucciones de instalación específicas para tu distribución de Linux ([Docker Engine](https://docs.docker.com/engine/install/) o [Docker Desktop para Linux](https://www.docker.com/products/docker-desktop)).

3. **Instalar Docker Desktop:**
   - **Windows:** Haz doble clic en el instalador y sigue las instrucciones para instalar Docker Desktop.
   - **macOS:** Haz doble clic en el archivo `.dmg` descargado, arrastra Docker a Aplicaciones y sigue las instrucciones de instalación.
   - **Linux:** Sigue las instrucciones de instalación proporcionadas para tu distribución.

4. **Iniciar Docker:**
   - **Windows y macOS:** Docker Desktop debería iniciarse automáticamente después de la instalación.
   - **Linux:** Inicia el servicio Docker usando `sudo systemctl start docker` y habilítalo para que se inicie en el arranque usando `sudo systemctl enable docker`.

5. **Verificar la Instalación:**
   Abre una terminal o ventana de comandos y ejecuta `docker --version` para verificar que Docker se haya instalado correctamente. También ejecuta `docker run hello-world` para verificar que Docker pueda descargar y ejecutar imágenes.
